#!/bin/sh

# Páginas
FOLDERS="sobre software livros-mat"

mkdir public/

for FOLDER in ${FOLDERS}; do
  mkdir public/${FOLDER};
done

## Títulos
sed 's/$title/Posts/' elementos/head.html >> public/index.html
sed 's/$title/Sobre/' elementos/head.html >> public/sobre/index.html
sed 's/$title/Livros de Matemática Livres/' elementos/head.html >> public/livros-mat/index.html
sed 's/$title/Use Softwares Éticos/' elementos/head.html >> public/software/index.html

cat elementos/header.html pages/posts.html elementos/footer.html >> public/index.html

for FOLDER in ${FOLDERS}; do
  cat elementos/header.html pages/${FOLDER}.html elementos/footer.html >> public/${FOLDER}/index.html;
done

# CSS
cp css/* public/
cp fork-awesome public/fork-awesome -r

# Imagens
cp images/ public/ -r

# Documentos
cp documents/ public/ -r

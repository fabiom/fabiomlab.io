#!/bin/sh

# Páginas
FOLDERS="about sobre software livros-mat"

mkdir public/
cp pages/redirect.html public/index.html
cp pages/redirect.html public/404.html

for F in ${FOLDERS}; do
  mkdir public/$F;
  cp pages/redirect.html public/$F/index.html
done

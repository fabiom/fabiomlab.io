---
title: "Use Softwares Éticos!"
---

### Uma lista de [softwares livres](https://www.gnu.org/philosophy/free-sw.pt-br.html), que respeitam sua privacidade e liberdade


<span style="font-size:14px">Nós vivemos em uma sociedade globalizada cada vez mais integrada ao mundo digital. A tecnologia avança cada vez mais rápido, e como consequência nós ficamos sem tempo para discutir as consequências éticas e sociais dos dispositivos e dos softwares que usamos. As implicações desse uso compulsivo e indiscriminado de novas tecnologias tem começado a aparecer. Edward Snowden nos mostrou que os governos estão sendo vigilantes de populações inteiras, e têm acesso a todas as nossas conversas privadas. As empresas que exploram nossos dados também estão aparecendo: Google, Facebook, Microsoft; são empresas tentam armazenar o máximo possível de informações do usuário, e vender essas informações pessoais para empresas que fazem propaganda direcionada. A melhor forma de alcançar uma mudança é através de reformas políticas e sociais. Para isso é importante que troquemos os softwares proprietários, centralizados e capitalistas por alternativas livres, éticas, comunitárias, descentralizadas e que respeitem a privacidade do usuário. O objetivo desta página é listar algumas dessas alternativas, e também ferramentas que sejam úteis para se obter privacidade on-line.</span>

---
<br/>

### <i class="fa fa-desktop" aria-hidden="true"></i>&nbsp; Sistemas Operacionais

- __[Debian](https://www.debian.org/):__ um sistema operacional livre, estável e seguro, criado por uma comunidade que se preocupa com a ética; todos os contribuidores do Debian devem concordar com um [contrato social](https://www.debian.org/social_contract)
- __[Tails](https://tails.boum.org/):__ um sistema operacional baseado no Debian, para se instalar em CDs e pen-drives. O Tails faz todas as suas conexões à internet através da rede [Tor](https://www.torproject.org/), e portanto permite a navegação anônima e que os usuários de países sem liberdade escapem da censura. Além disso, Tails inclue por padrão várias ferramentas criptográficas e não deixa rastros no computador. Foi inclusive [usado por Edward Snowden](http://www.wired.com/2014/04/tails) para evitar o rastramento da NSA

### <i class="fa fa-globe" aria-hidden="true"></i>&nbsp; Navegadores de Internet

- __[Qupzilla](https://www.qupzilla.com/):__ um navegador simples, livre, com um bloqueador de propagandas integrado, e que respeita os usuários
- __[Waterfox](https://www.waterfoxproject.org/):__ uma versão modificada do Firefox, que remove recursos indesejados, e que procura respeitar a privacidade dos usuários
- __[ungoogled-chromium](https://github.com/Eloston/ungoogled-chromium):__ uma modificação do Google Chrome para remover integração com o Google e melhorar privacidade, controle e transparência
- __[Epiphany](https://wiki.gnome.org/Apps/Web):__ o navegador padrão do projeto GNOME; simples e com um bloqueador de propagandas integrado
- __[GNU Icecat](https://www.gnu.org/software/icecat/):__ um navegador criado pelo projeto [GNU](https://gnu.org/), com base no Firefox, que inclui várias configurações para respeitar sua privacidade, e bloqueia JavaScript não-livre
- __[Tor Browser](https://www.torproject.org/):__ um navegador baseado no Firefox que fornece total anonimato, encriptando seus dados e fazendo-os passar por outros endereços de IP antes de chegar no seu computador. Os provedores de acesso a internet também ficam incapazes de rastrear as páginas que os usuários acessam. A navegação pode ser mais lenta neste navegador
- __[Privacy Browser](https://www.stoutner.com/privacy-browser/):__  um navegador para Android com foco em privacidade e seguraça, com opções de desabilitar cookies e javascript

### <i class="fa fa-puzzle-piece" aria-hidden="true"></i>&nbsp; Extensões para Navegadores
Nota: extensões para o Firefox funcionam com Waterfox, GNU Icecat e Tor Browser. Extensões para Chrome funcionam com ungoogled-chromium

- __[uBlock Origin](https://github.com/gorhill/uBlock):__ bloqueador de propagandas e de rastreadores extremamente leve, e simples. Vem por padrão com uma grande e abrangente lista de filtros que podem ser ativados para bloqueio. <i class="fa fa-firefox" aria-hidden="true"></i> <i class="fa fa-chrome" aria-hidden="true"></i> <i class="fa fa-opera" aria-hidden="true"></i> <i class="fa fa-edge" aria-hidden="true"></i>
- __[Privacy Badger](https://www.eff.org/privacybadger):__ uma extensão criada pela fundação sem fins luctrativos [EFF](https://www.eff.org/), que bloqueia elementos e propagandas que rastreiam os usuários. <i class="fa fa-firefox" aria-hidden="true"></i> <i class="fa fa-chrome" aria-hidden="true"></i> <i class="fa fa-opera" aria-hidden="true"></i>
- __[HTTPS Everywhere](https://www.eff.org/https-everywhere):__ uma extensão criada pela [EFF](https://www.eff.org/) que obriga o navegador a acessar sites utilizando o protocolo HTTPS ao invés de HTTP (quando existir essa opção). Isso aumenta muito a segurança dos usuários, pois os sites HTTPS são criptografados desde o servidor até o seu computador, impedindo a interceptação dos dados, e utilizam um sistema de certificados e assinaturas digitais que garente a autenticidade da página. <i class="fa fa-firefox" aria-hidden="true"></i> <i class="fa fa-chrome" aria-hidden="true"></i> <i class="fa fa-opera" aria-hidden="true"></i>

### <i class="fa fa-folder" aria-hidden="true"></i>&nbsp; Sincronização de Arquivos

- __[Syncthing](https://syncthing.net/):__ um programa de sincronização de arquivos descentralizado, rápido e que encripta os dados durante a tranferência. Disponível para Windows, macOS, Linux e Android
- __[NextCloud](https://nextcloud.com/):__ um programa de nuvem pessoal segura e privada. É possível baixar e hospedar em seu próprio servidor ou utilizar um dos [provedores](https://nextcloud.com/providers/)

### <i class="fa fa-users" aria-hidden="true"></i>&nbsp; Redes Sociais

- __[Mastodon](https://joinmastodon.org):__ rede social livre, federada, semelhante ao twitter. Uma lista de instâncias disponíveis está disponível em [instances.social/list](https://instances.social/list)
- __[diaspora*](https://diasporafoundation.org/):__ uma rede social, semelhante ao facebook, mas totalmente descentralizada. Isso significa que não existe um servidor central, mas vários servidores ("pods") ao redor do mundo que se comunicam. Você pode criar uma conta no pod que preferir, ou até criar o seu próprio, e poderá normalmente se comunicar com pessoas de outros pods. No Brasil, os dois principais pods são [diasporabr.com.br](https://diasporabr.com.br/) e [diasporabrazil.org](https://diasporabrazil.org/). Esta rede social também é software livre, encriptada, e respeita a privacidade do usuário
- __[GNU social](https://gnu.io/social/):__ uma rede social, semelhante ao twitter, mas totalmente descentralizada. Criada pelo projeto [GNU](https://gnu.org/), você pode adicionar uma conta no servidor que quiser (inclusive criar seu próprio servidor) e se comunicar com usuários de outros servidores. Alguns servidores abertos são [Quitter.se](https://quitter.se/), [Quitter.no](https://quitter.no/), [LoadAverage](https://loadaverage.org), e no Brasil existe o servidor do projeto LibrePlanet Brasil em [social.libreplanetbr.org](http://social.libreplanetbr.org/) (mais informações e criação de contas [aqui!](http://wiki.libreplanetbr.org/Servi%C3%A7os/))

### <i class="fa fa-comments" aria-hidden="true"></i>&nbsp; Comunicação

- __[Tox](https://tox.chat/):__ um programa de troca de mensagens e ligações descentralizado, seguro e desenvolvido pela própria comunidade (Linux/macOS/Windows/Android/iOS)
- __[Ring](https://ring.cx/):__ um programa de troca de mensagens e ligações descentralizado e seguro, que suporta múltiplos dispositivos (Linux/macOS/Windows/Android)
- __[Signal](https://signal.org/):__ um aplicativo de mensagens e ligações criptografadas via internet, que utiliza uma criptografia reconhecida academicamente e comprovadamente segura [[download do .apk]](https://signal.org/android/apk/) (Android/iOS)
- __[Briar](https://briarproject.org/):__ um aplicativo de mensagens descentralizado, criptografado e que funciona via rede Tor ou Bluetooth. Ainda está em desenvolvimento, e um repositório com a versão inclompleta e instável pode ser encontrado [aqui](https://grobox.de/fdroid/)


### <i class="fa fa-android" aria-hidden="true"></i>&nbsp; Aplicativos de Android

- __[F-Droid](https://f-droid.org/):__ uma loja de aplicativos alternativa, desenvolvida pela comunidade, que distribui apenas aplicativos software livre, e avisa sobre eventuais [anti-recursos](https://f-droid.org/wiki/page/Antifeatures)
- __[Yalp Store](https://github.com/yeriomin/YalpStore):__ um programa que permite baixar aplicativos da loja Google Play, sem a necessidade de se instalar os serviços proprietários da Google

### <i class="fa fa-search" aria-hidden="true"></i>&nbsp; Mecanismos de Pesquisa

- __[DuckDuckGo](https://duckduckgo.com/):__ um mecanismo de pesquisa que [não rastreia os usuários](http://donttrack.us/), que não grava informações pessoais como endereço de IP, e que [não filtra os resultados](http://dontbubble.us/) de acordo com o usuário como o Google faz. Parte dos lucros do buscador são convertidos em doações para projetos de software livre escolhidos pela comunidade
- __[StartPage](https://startpage.com/):__ um mecanismo de pesquisa que fornece os mesmos resultados que o Google, mas sem rastrear os usuários, sem filtrar os resultados e respeitando a privacidade

### <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp; Serviços de Email

- __[RiseUp](https://riseup.net/):__ um serviço que fornece emails, listas de emails, VPN, chat e armazenamento de arquivos para pessoas e grupos trabalhando em mudanças sociais liberatórias. Os dados dos usuários são guardados criptografados, e eles dependem de doações para manter o serviço funcionando; servidores nos EUA
- __[Disroot](https://disroot.org/):__ fornece emails, nuvem, e serviços online; são baseados nos princípios de liberdade, privacidade, federação e descentralização; servidores na Holanda
- __[Tutanota](https://tutanota.com/):__ serviço de emails criptografado, gratuito, e que utiliza software livre; servidores na Alemanha

### <i class="fa fa-envelope-square" aria-hidden="true"></i>&nbsp; Clientes de Email

Para maior segurança e privacidade no envio e recebimento de emails, é recomendada a utilização de [criptografia PGP](https://emailselfdefense.fsf.org/pt-br/).

- __[Claws Mail](http://www.claws-mail.org/):__ um programa leve, rápido, simples e muito customizável para gerenciar emails, com suporte PGP (Linux/Windows)
- __[Thunderbird](https://www.thunderbird.net/):__ software da Mozilla simples para gerenciar emails. A extensão [Enigmail](https://www.enigmail.net/) fornece suporte a PGP (Linux/Mac/Windows)
- __[K-9 Mail](https://k9mail.github.io/):__ um aplicativo de Android para gerenciar email, com suporte a PGP

### <i class="fa fa-file-text" aria-hidden="true"></i>&nbsp; Editores de Documentos

- __[LibreOffice](https://pt-br.libreoffice.org/):__ um conjunto de programas livre para editar documentos de texto, planilhas, apresentações, desenhos e bancos de dados. São desenvolvidos pela [The Document Foundation](https://www.documentfoundation.org/), uma fundação sem fins lucrativos (Linux/macOS/Windows)

### <i class="fa fa-key-modern" aria-hidden="true"></i>&nbsp; Gereciadores de Senhas

- __[KeePassXC](https://www.keepassxc.org/):__ um gerenciador de senhas offline que criptografa todas as senhas utilizando uma senha-mestra. Possui ainda um gerador de senhas aleatórias, permitindo que o usuário tenha uma senha diferente para cada serviço, e realiza preenchimento automático destas senhas no navegador
- __[KeePassDroid](https://f-droid.org/packages/com.android.keepass/):__ uma versão para Android dos gerenciadores de senhas KeePass/KeePassXC

### <i class="fa fa-play-circle" aria-hidden="true"></i>&nbsp; Reprodutores de Vídeos

- __[mpv](https://mpv.io/):__ um tocador de vídeos leve e minimalista, mas poderoso; com comandos pelo teclado (Linux/macOS/Windows/Android)
- __[VLC](https://www.videolan.org/vlc/):__ um tocador de vídeos completo, com muitos recursos e simples de utilizar (Linux/macOS/Windows/Android/iOS)
- __[youtube-dl](https://rg3.github.io/youtube-dl/download.html):__ um baixador de vídeos de [vários sites](https://rg3.github.io/youtube-dl/supportedsites.html), com muitas opções de download (Linux/macOS/Windows)
- __[NewPipe](https://newpipe.schabi.org/):__ um aplicativo de Android que reproduz e baixa vídeos do YouTube, com muitas opções e recursos

### <i class="fa fa-map" aria-hidden="true"></i>&nbsp; Mapas
- __[OpenStreetMap](https://www.openstreetmap.org/):__ um mapa aberto, completo e contruído pela comunidade, que pode ser acessado pelo site ou por outros softwares
- __[OsmAnd](https://wiki.openstreetmap.org/wiki/OsmAnd):__ um aplcativo livre de mapas, que obtém dados do OpenStreetMap e da Wikipedia, com funcionalidade de mapas offline. Disponível da F-Droid como [OsmAnd~](https://f-droid.org/en/packages/net.osmand.plus/) (Android/iOS)
- __[Gnome Maps](https://wiki.gnome.org/Apps/Maps):__ um programa de mapas e navegação, que obtém dados do OpenStreetMap (Linux)

### <i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp; Editores de Imagens

- __[GIMP](https://www.gimp.org/):__ um editor de imagens e projetos gráficos livre e poderoso, com muitos [tutoriais](https://www.gimp.org/tutorials/) (Linux/macOS/Windows)
- __[InkScape](https://inkscape.org/pt-br/):__ uma ferramenta livre para edição de imagens e documentos vetoriais (Linux/macOS/Windows)

<br/>

## Páginas similares

Me inspirei em outras páginas para construir esta. Aqui estão algumas delas.

- [Prism Break](https://prism-break.org/en/)
- [Privacy Tools](https://www.privacytools.io/)
- [Tem boi na linha?](http://www.temboinalinha.org/)
- [Security in a Box](https://securityinabox.org/pt/)
